USE [prueba1]
GO
/****** Object:  Table [dbo].[mascota]    Script Date: 09/03/2015 22:34:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[mascota](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[idPersona] [bigint] NOT NULL,
 CONSTRAINT [PK_mascota] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[persona]    Script Date: 09/03/2015 22:34:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[persona](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[apellido] [varchar](50) NOT NULL,
	[telefono] [varchar](50) NOT NULL,
 CONSTRAINT [PK_persona] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[mascota] ON 

INSERT [dbo].[mascota] ([id], [nombre], [idPersona]) VALUES (1, N'fluflu', 5)
INSERT [dbo].[mascota] ([id], [nombre], [idPersona]) VALUES (2, N'boby', 1)
INSERT [dbo].[mascota] ([id], [nombre], [idPersona]) VALUES (3, N'perla', 4)
SET IDENTITY_INSERT [dbo].[mascota] OFF
SET IDENTITY_INSERT [dbo].[persona] ON 

INSERT [dbo].[persona] ([id], [nombre], [apellido], [telefono]) VALUES (1, N'jhon', N'salguero', N'75581594')
INSERT [dbo].[persona] ([id], [nombre], [apellido], [telefono]) VALUES (2, N'luis', N'veizaga', N'78963214')
INSERT [dbo].[persona] ([id], [nombre], [apellido], [telefono]) VALUES (3, N'pedro', N'lujan', N'78963258')
INSERT [dbo].[persona] ([id], [nombre], [apellido], [telefono]) VALUES (4, N'maria', N'perez', N'85478963')
INSERT [dbo].[persona] ([id], [nombre], [apellido], [telefono]) VALUES (5, N'nicol', N'justiniano', N'75350135')
SET IDENTITY_INSERT [dbo].[persona] OFF
ALTER TABLE [dbo].[mascota]  WITH CHECK ADD  CONSTRAINT [FK_mascota_persona] FOREIGN KEY([idPersona])
REFERENCES [dbo].[persona] ([id])
GO
ALTER TABLE [dbo].[mascota] CHECK CONSTRAINT [FK_mascota_persona]
GO

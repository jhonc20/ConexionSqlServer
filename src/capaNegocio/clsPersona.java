/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package capaNegocio;

import capaDatos.conexion;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import java.lang.String;

public class clsPersona extends conexion {
    long id;
    String nombre,apellido,telefono;
    public clsPersona()
    {
        id = 0;
        nombre = "";
        apellido = "";
        telefono = "";
    }
    public clsPersona(String idPersona,String nombrePersona,String apellidoPersona,String telefonoPersona)
    {
        id = Long.valueOf(idPersona);
        nombre = nombrePersona;
        apellido = apellidoPersona;
        telefono = telefonoPersona;
    }
    public void setId(long idPersona)
    {
        id = idPersona;
    }
    public long getId()
    {
        return id;
    }
    public void setNombre(String nombrePersona)
    {
        nombre = nombrePersona;
    }
    public String getNombre()
    {
        return nombre;
    }
    public void setApellido(String apellidoPersona)
    {
        apellido = apellidoPersona;
    }
    public String getApellido(String apellidoPersona)
    {
        return apellido;
    }
    public void setTelefono(String telefonoPersona)
    {
        telefono = telefonoPersona; 
    }
    public String getTelefono()
    {
        return telefono;
    }
    public void registroPersona()
    {
        try
        {
            Statement st = conectarBD().createStatement();
            st.executeUpdate("insert into persona values('"+nombre+"',"+"'"+apellido+"',"+"'"+telefono+"'"+")");
        }
        catch(SQLException ex)
        {
            JOptionPane.showMessageDialog(null, "Error no se Insertaron los datos",ex.getMessage(),JOptionPane.INFORMATION_MESSAGE);
        }
    }
    @Override
    public String toString()
    {
        return nombre+" "+apellido;
    }
    public void ActualizarPersona()
    {
        try
        {
            Statement st = conectarBD().createStatement();
            st.executeUpdate("update persona set nombre='"+nombre+"',apellido="+"'"+apellido+"',telefono="+"'"+telefono+"' where id ="+id);
        }
        catch(SQLException ex)
        {
            JOptionPane.showMessageDialog(null, "Error al Modificar los datos",ex.getMessage(),JOptionPane.INFORMATION_MESSAGE);
        }
    }
    public ResultSet datosPersona()
    {
        try
        {
            Statement st = conectarBD().createStatement();
            ResultSet datos = st.executeQuery("select * from persona ORDER BY id ASC");
            return datos;
        }
        catch(SQLException ex)
        {
            return null;
        }
    }
}



package capaNegocio;

import capaDatos.conexion;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

public class clsMascota extends conexion {
    long id,idPersona;
    String nombre;
    public clsMascota()
    {
        id = 0;
        idPersona = 0;
        nombre = "";
    }
    public void setId(long idMascota)
    {
        id=idMascota;
    }
    public long getId()
    {
        return id;
    } 
    public void setIdPersona(long idPersonaMascota)
    {
        idPersona=idPersonaMascota;
    }
    public long getIdPersona()
    {
        return idPersona;
    }
    public void setNombre(String nombreMascota)
    {
        nombre=nombreMascota;
    }
    public String getNombre()
    {
        return nombre;
    }
    public void registroMascota()
    {
        try
        {
            Statement st = conectarBD().createStatement();
            st.executeUpdate("insert into mascota values('"+nombre+"',"+idPersona+")");
        }
        catch(SQLException ex)
        {
            JOptionPane.showMessageDialog(null, "Error no se Insertaron los datos",ex.getMessage(),JOptionPane.INFORMATION_MESSAGE);
        }
    }
    public void ActualizarMascota()
    {
        try
        {
            try (Connection con = conectarBD()) {
                Statement st = con.createStatement();
                st.executeUpdate("update mascota set nombre='"+nombre+"',idPersona="+idPersona+ "where id ="+id);
            }
        }
        catch(SQLException ex)
        {
            JOptionPane.showMessageDialog(null, "Error al Modificar los datos",ex.getMessage(),JOptionPane.INFORMATION_MESSAGE);
        }
    }
    public ResultSet datosMascota()
    {
        try
        {
            Statement st = conectarBD().createStatement();
            ResultSet datos = st.executeQuery("select mascota.id,mascota.nombre,CONCAT(persona.nombre,' ',persona.apellido) as Dueño,persona.id from mascota inner join persona on mascota.idPersona=persona.id ORDER BY mascota.id ASC");
            return datos;
        }
        catch(SQLException ex)
        {
            return null;
        }
    }
}
